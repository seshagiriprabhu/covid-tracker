#COVID Tracker

## To test the site locally
### Linux Requirements
```bash
sudo apt-get install ruby ruby-dev make gcc
```

### Ruby Requirements
```bash
sudo gem install jekyll bundler
```

### Update the bundle
```bash
bundle update
```

### Serve the static site locally
```bash
bundle exec jekyll serve
```
You will be able to open the site in
[http://127.0.0.1:4000](http://127.0.0.1:4000)


## Deploy
If you fork this repository as a Gitlab project, change these lines in `_config.yml` files:

```yml
baseurl: "/covid-tracker"                # PROJECT_SLUG
url: "https://seshagiriprabhu.gitlab.io" # USERNAME.gitlab.io
```
